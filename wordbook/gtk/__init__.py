#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2016-2020 Mufeed Ali
# SPDX-License-Identifier: GPL-3.0-only or GPL-3.0-or-later
# Author: Mufeed Ali <fushinari@protonmail.com>

"""
Wordbook is a dictionary application made with Python and GTK 3.

It's a simple script basically. It uses existing tools and as such, easily
works across most Linux distributions without any changes.
"""
from .main import main

if __name__ == '__main__':
    main()
